const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const SubscriptionSchema = new Schema({ 
  organization: { type: String, index: true, required: true },
  maximumAllowedMinutes: { type: Number, default: 0 },
  consumedMinutes: { type: Number, default: 0 },

}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } })

const NoiseCancellationItemSchema = new Schema({
  rawUrl: { type: String },
  rawKey: { type: String },

  processedUrl: { type: String},
  processedKey: { type: String },
  status: { type: String, enum: ['processing', 'done', 'failed' ]},
  minutesDuration: { type: Number },

  created_at: { type: Number, default: Date.now, index: true },
  updated_at: { type: Number, default: Date.now },
})

const Subscription = mongoose.model('subscription', SubscriptionSchema)
const NoiseCancellationItem = mongoose.model('noiseCancellationItem', NoiseCancellationItemSchema);

module.exports = { Subscription, NoiseCancellationItem };
