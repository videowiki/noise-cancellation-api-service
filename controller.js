const fs = require('fs');
const path = require('path');
const uuid = require('uuid').v4;
const { Subscription, NoiseCancellationItem } = require('./models');
const { audioProcessor } = require('./workers');
const { storageService } = require('./services');
const utils = require('./utils');
const NOISECANCELLATION_RAW_DIRECTORY = 'noise_cancellation_api/raw';
const NOISECANCELLATION_PROCESSED_DIRECTORY = 'noise_cancellation_api/processed';
const DEFAULT_AUDIO_FORMAT = 'mp3'

module.exports = {
    processAudio: function (req, res) {
        const { outputFormat } = req.body;
        const organizationId = req.user ? req.user.organizationRoles[0].organization._id : null;
        const file = req.files && req.files.length > 0 ? req.files[0] : null
        if (!file) return res.status(400).send('Empty file');
        // Process audio
        console.log(file)
        console.log('output format', outputFormat)
        let tmpFiles = [];
        const filePath = file.path
        tmpFiles.push(filePath)
        processAudio({ filePath, outputFormat: outputFormat || DEFAULT_AUDIO_FORMAT })
        .then(clearedFilePath => {
            return res.sendFile(clearedFilePath, (err) => {
                if (err) {
                    console.log('error sending back file', err);
                }
                tmpFiles.push(clearedFilePath);
                tmpFiles.forEach(f => fs.unlink(f, () => { }))
            });
        })
        .catch(err => {
            // add audio duration to the organization's quota since it failed
            utils.getFileDuration(filePath)
            .then(duration => {
               return addAudioDurationToQuota(duration / 60, organizationId)
            })
            .then(() => {
                tmpFiles.forEach(f => fs.unlink(f, () => { }))
            })
            .catch(err => {
                console.log('error add to quota', err);
                tmpFiles.forEach(f => fs.unlink(f, () => { }))
            });
            console.log(err);
            return res.status(400).send('Something went wrong');
        });
    },

    processVideo: function (req, res) {
        const file = req.files && req.files.length > 0 ? req.files[0] : null
        if (!file) return res.status(400).send('Empty file');
        const organizationId = req.user ? req.user.organizationRoles[0].organization._id : null;
        // Extract audio
        const tmpFiles = [file.path];
        let audioPath;
        utils.extractAudioFromVideo(file.path, DEFAULT_AUDIO_FORMAT)
        .then(p => {
            audioPath = p;
            tmpFiles.push(audioPath)
            return processAudio({ filePath: audioPath, outputFormat: DEFAULT_AUDIO_FORMAT })
        })
        .then((clearedPath) => {
            tmpFiles.push(clearedPath);
            return utils.burnAudioToVideo(clearedPath, file.path)
        })
        .then((finalPath) => {
            tmpFiles.push(finalPath);
            return res.sendFile(finalPath, (err) => { 
                if (err) {
                    console.log(err);
                }
                tmpFiles.forEach(f => {
                    fs.unlink(f, () => {});
                })
            })
        })
        .catch(err => {

            utils.getFileDuration(audioPath)
            .then((duration) => {
                return addAudioDurationToQuota(duration / 60, organizationId)
            })
            .then(() => {
                tmpFiles.forEach(f => {
                    fs.unlink(f, () => {});
                })
            })
            .catch(err => {
                console.log(err);
                tmpFiles.forEach(f => {
                    fs.unlink(f, () => {});
                })
            })
            console.log(err);
            return res.status(400).send('Something went wrong');
        })
        // Process audio
    },
}

function processAudio({ filePath, outputFormat }) {
    return new Promise((resolve, reject) => {
        // Upload file 
        const clearedFileName = `cleared_audio_${uuid()}.mp3`;
        let clearedFilePath = path.join(__dirname, clearedFileName);
        let noiseCancellationItem;
        storageService.saveFile(NOISECANCELLATION_RAW_DIRECTORY, `${uuid()}.${filePath.split('.').pop()}`, fs.createReadStream(filePath))
            .then((res) => {
                return NoiseCancellationItem.create({ rawUrl: res.url, rawKey: res.Key, status: 'processing' })
            })
            // Process audio
            .then((doc) => {
                noiseCancellationItem = doc.toObject();
                return audioProcessor.processRecordedAudioViaApi({ url: noiseCancellationItem.rawUrl, outputFormat })
            })
            .then(fileBuffer => {
                return new Promise((resolve, reject) => {
                    fs.writeFile(clearedFilePath, fileBuffer, (err) => {
                        if (err) return reject(err);
                        return resolve(clearedFilePath);
                    })
                })
                .then(filePath => {
                    if (filePath) {
                        // Return respose
                        resolve(filePath)
                        // Upload returned response
                        return storageService.saveFile(NOISECANCELLATION_PROCESSED_DIRECTORY, clearedFileName, fs.createReadStream(clearedFilePath))
                    }
                })
                .then(uploadRes => {
                    // Store prcessed item
                    return NoiseCancellationItem.findByIdAndUpdate(noiseCancellationItem._id, { $set: { processedUrl: uploadRes.url, processedKey: uploadRes.Key, status: 'done' } })
                })
                .then(() => {
    
                })
                .catch(err => {
                    console.log(err);
                })
            })
            .catch((err) => {
                reject(err);
                NoiseCancellationItem.findByIdAndUpdate(noiseCancellationItem._id, { $set: { status: 'failed' } })
            })

    })
}

function addAudioDurationToQuota(duration, organization) {
    return new Promise((resolve, reject) => {
        Subscription.update({ organization }, { $inc: { consumedMinutes: +duration }})
        .then(() => {
            resolve();
            Subscription.find({ organization })
            .then(s => {
                console.log('quota change', s, duration)
            })
            .catch(err => {
                console.log(err)
            })
        })
        .catch(reject)
    })
}