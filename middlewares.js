
const { Subscription } = require('./models');
const utils = require('./utils');

module.exports = {
    validateSubscriptionQuota: function(req, res, next) {
        const user = req.user;
        const file = req.files && req.files.length > 0 ? req.files[0] : null
        
        if (!user) return res.status(401).send('Unauthorized');
        if (!file) return res.status(400).send('Empty file');

        let organizationId;
        try {
            organizationId = req.user.organizationRoles[0].organization._id;
        } catch (e) {
            console.log(e);
        }
        if (!organizationId) {
            return res.status(403).send('Forbidden');
        }
        console.log('middleware')
        Subscription.find({ organization: organizationId })
        .then(subscriptions => {
            if (!subscriptions || subscriptions.length === 0) {
                return res.status(403).send('Forbidden');
            }
            const subscription = subscriptions[0];
            utils.getFileDuration(file.path)
            .then(duration => {
                const durationInMinutes = duration / 60;
                if (subscription.maximumAllowedMinutes < (subscription.consumedMinutes + durationInMinutes)) {
                    return res.status(400).send('Maximum quota allowance has been consumed, please contact support!')
                }
                
                subtractAudioDurationFromQuota(durationInMinutes, organizationId)
                .then(() => {
                })
                .catch(() => {
                })
                return next();
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send('Something went wrong');
            })
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send('Something went wrong');
        })
    }
}
// Subscription.create({ organization: "5e83e69e69c5a0001e2b9566", maximumAllowedMinutes: 5, consumedMinutes: 0 })



function subtractAudioDurationFromQuota(duration, organization) {
    return new Promise((resolve, reject) => {
        Subscription.update({ organization }, { $inc: { consumedMinutes: +duration }})
        .then(() => {
            resolve();
            Subscription.find({ organization })
            .then(s => {
                console.log('quota change', s, duration)
            })
            .catch(err => {
                console.log(err)
            })
        })
        .catch(reject)
    })
}