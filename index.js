const mongoose = require('mongoose');
const DB_CONNECTION = process.env.NOISE_CANCELLATION_API_SERVICE_DATABASE_URL;

const vwGenerators = require('@videowiki/generators');
const { server, app } = vwGenerators.serverGenerator({ uploadLimit: '500mb' })
const controller = require('./controller');
const middlewares = require('./middlewares')

const multer = require('multer');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '/tmp')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + '.' + file.originalname.split('.').pop())
    }
})
const upload = multer({ storage: storage })

let mongoConnection
mongoose.connect(DB_CONNECTION)
.then(con => {
    mongoConnection = con.connection;
    con.connection.on('disconnected', () => {
        console.log('Database disconnected! shutting down service')
        process.exit(1);
    })

    vwGenerators.healthcheckRouteGenerator({ router: app, mongoConnection })    

    app.all('*', (req, res, next) => {
        if (req.headers['vw-user-data']) {
            try {
                const user = JSON.parse(req.headers['vw-user-data']);
                req.user = user;
            } catch (e) {
                console.log(e);
            }
        }
        next();
    })

    app.get('/', (req, res) => {
        res.send('OK')
    })
    app.post('/audio', upload.any(), middlewares.validateSubscriptionQuota, controller.processAudio);
    // app.post('/video', upload.any(), controller.processVideo)


    // curl -X POST -H "vw-x-user-api-key: fa551908-2bd3-46dc-86b9-99482e11a7c3-1587159599718" -H "vw-x-user-api-key-secret: 4ba75eb0-a558-4047-9438-e7628f6273a8-1587159599718" -F "outputFormat=mp3" -F file=@/home/hassan/Downloads/audio-930a4b9b-ac1a-4295-8707-50747400a6a2.mp3 http://localhost:4000/api/noiseCancellation/audio 


})
.catch(err => {
    console.log('error mongo connection', err);
    process.exit(1);
})
// app.use('/db', vwGenerators.mongodbCRUDGenerator({ Model, Router: createRouter() }))


const PORT = process.env.PORT || 4000;
server.listen(PORT)
console.log(`Magic happens on port ${PORT}`)       // shoutout to the user
console.log(`==== Running in ${process.env.NODE_ENV} mode ===`)
exports = module.exports = app             // expose app
