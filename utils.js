const { exec } = require('child_process')
const uuid = require('uuid').v4;
const path = require('path');

module.exports = {
    extractAudioFromVideo(videoPath, audioFormat) {
        return new Promise((resolve, reject) => {
            const audioPath = path.join(__dirname, `audio_${uuid()}.${audioFormat}`);
            console.log('extracting audio')
            const cmd = `ffmpeg -i ${videoPath} ${audioPath}`;
            exec(cmd, (err) => {
                console.log('extracted audio')
                if (err) return reject(err);
                return resolve(audioPath);
            })
        })
    },
    burnAudioToVideo(audioPath, videoPath) {
        return new Promise((resolve, reject) => {
            const finalPath = path.join(__dirname, `video_${uuid()}.${videoPath.split('.').pop()}`);
            const cmd = `ffmpeg -i ${videoPath} -i ${audioPath} -map 0:v -map 1:a -c copy ${finalPath}`

            exec(cmd, (err) => {
                if (err) return reject(err);
                return resolve(finalPath);
            })
        })
    },
    getFileDuration(url) {
        return new Promise((resolve, reject) => {
            exec(`ffprobe -i ${url} -show_entries format=duration -v quiet -of csv="p=0"`, (err, stdout, stderr) => {
                if (err) {
                    return reject(err);
                }
                if (stderr) {
                    return reject(stderr);
                }
                const duration = parseFloat(stdout.replace('\\n', ''));
                resolve(duration);
            })
        })
    }
}