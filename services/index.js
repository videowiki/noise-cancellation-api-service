const {
    STORAGE_SERVICE_API_ROOT,
} = process.env;

const storageService = require('@videowiki/services/storage')(STORAGE_SERVICE_API_ROOT);

module.exports = {
    storageService,
}